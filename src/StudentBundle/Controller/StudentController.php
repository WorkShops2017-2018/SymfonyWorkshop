<?php

namespace StudentBundle\Controller;

use StudentBundle\Entity\Student;
use StudentBundle\Form\EtudiantType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class StudentController extends Controller
{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $etudiants = $em->getRepository("StudentBundle:Student")->findAll();
        return $this->render("@Student/index.html.twig", array("students" => $etudiants));
    }

    public function addAction(Request $request)
    {
        $etudiant = new Student();
        $form = $this->createForm(EtudiantType::class, $etudiant);
        $form->add('Add', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($etudiant);
            $em->flush();
            return $this->redirectToRoute('student_all');
        }
        return $this->render("@Student/add.html.twig", array('form' => $form->createView()));
    }

    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiant = $em->getRepository("StudentBundle:Student")->find($id);
        $form = $this->createForm(EtudiantType::class, $etudiant);
        $form->add('Edit', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($etudiant);
            $em->flush();
            return $this->redirectToRoute('student_all');
        }
        return $this->render("@Student/add.html.twig", array('form' => $form->createView()));
    }

    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiant = $em->getRepository("StudentBundle:Student")->find($id);
        $em->remove($etudiant);
        $em->flush();
        return $this->redirectToRoute('student_all');
    }
}
