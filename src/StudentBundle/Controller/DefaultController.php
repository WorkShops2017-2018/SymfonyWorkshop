<?php

namespace StudentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('StudentBundle:Default:index.html.twig');
    }
    public function aaAction()
    {
        return $this->render('index.html.twig');
    }
}
