<?php
/**
 * Created by PhpStorm.
 * User: Bassem
 * Date: 06/12/2017
 * Time: 15:40
 */

namespace StudentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="c")
 */
class Classe
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string",length=255,nullable=false)
     */
    private $nom;
    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="StudentBundle\Entity\Student", mappedBy="Classe")
     */
    private $etudiants;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getEtudiants()
    {
        return $this->etudiants;
    }

    /**
     * @param mixed $etudiants
     */
    public function setEtudiants($etudiants)
    {
        $this->etudiants = $etudiants;
    }



}