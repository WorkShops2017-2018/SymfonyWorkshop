<?php
/**
 * Created by PhpStorm.
 * User: Bassem
 * Date: 06/12/2017
 * Time: 15:40
 */

namespace StudentBundle\Entity;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="etudiant")
 */
class Student
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $cin;
    /**
     * @ORM\Column(type="string",length=255,nullable=false)
     */
    private $nom;

    /**
     * Many Users have One Departement.
     * @ORM\ManyToOne(targetEntity="StudentBundle\Entity\Classe",inversedBy="etudiants")
     * @ORM\JoinColumn(name="fk_classe", referencedColumnName="id")
     */
    private $classe;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * @param mixed $cin
     */
    public function setCin($cin)
    {
        $this->cin = $cin;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     * @param mixed $classe
     */
    public function setClasse($classe)
    {
        $this->classe = $classe;
    }

}