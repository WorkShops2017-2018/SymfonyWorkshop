<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17/01/18
 * Time: 02:44 م
 */

namespace StudentBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EtudiantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cin')
            ->add('nom')
            ->add('classe', EntityType::class, array(
                'class' => 'StudentBundle\Entity\Classe',
                'choice_label' => 'nom',
                'multiple' => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'student_bundle_etudiant_type';
    }
}
